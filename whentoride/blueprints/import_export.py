from flask import Blueprint, current_app as app
from flask.cli import with_appcontext

from ..models.bikeshare_system import BikeshareSystem


def log():
    return app.logger


bp = Blueprint("import-export", __name__, url_prefix="/export")
bp.cli.help = "Import and export commands"


@bp.cli.command("export")
@with_appcontext
def cli_export():
    db_systems = BikeshareSystem.query.all()
    output_json = [{
        'id': system.id,
        'name': system.name,
        'country_code': system.country_code,
        'location': system.location,
        'url': system.url,
        'gbfs_url': system.gbfs_url,
        'phone': system.phone,
        'email': system.email,
        'timezone': system.timezone,
        'stations': [
            {
                'id': station.id,
                'name': station.name,
                'address': station.address,
                'latitude': float(station.latitude),
                'longitude': float(station.longitude),
                'statuses': [
                    {
                        'time': status.time.isoformat(),
                        'is_returning': status.is_returning,
                        'is_renting': status.is_renting,
                        'is_installed': status.is_installed,
                        'docks_available': status.docks_available,
                        'bikes_available': status.bikes_available
                    } for status in station.statuses]
            } for station in system.stations]
    } for system in BikeshareSystem.query.all()]
    import json
    print(json.dumps(output_json))
