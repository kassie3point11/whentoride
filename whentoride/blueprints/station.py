import json
import logging
from collections import OrderedDict
from datetime import datetime, timedelta
from typing import List

import click
from flask import Blueprint, current_app as app, request, abort
from flask.cli import with_appcontext
from flask_sqlalchemy import BaseQuery

from ..db import db
from ..models.bikeshare_system import BikeshareSystem
from ..models.bikeshare_station import BikeshareStation
from ..models.bikeshare_station_status import BikeshareStationStatus
import dateutil.parser

from ..utility import split_by_comma


def log():
    return app.logger


bp = Blueprint("station", __name__, url_prefix="/station")
bp.cli.help = "Get data about known stations and fetch lists of stations"


def get_system_status(system, time = None):
    if time == None:
        time = datetime.now()
    if type(system) == str:
        system: BikeshareSystem = BikeshareSystem.query.get_or_404(system)
    log().info("getting station status for %s", system.id)
    status_feed = system.feed("station_status")['data']['stations']
    stations_in_feed = [sta['station_id'] for sta in status_feed]
    feed_stations_in_db = BikeshareStation.query.filter(BikeshareStation.id.in_(stations_in_feed)).all()
    found_stations = [sta.id for sta in feed_stations_in_db]
    missing_stations = [sta_id for sta_id in stations_in_feed if sta_id not in found_stations]
    if len(missing_stations) > 0:
        log().info("missing stations for %s, getting station info", system.id)
        found_stations = update_system_station_info(system)
    for station in status_feed:
        if station['station_id'] not in found_stations:
            log().warn("Station in station status but not in station info? %s", station['station_id'])
            continue
        status = BikeshareStationStatus()
        status.time = time
        status.station_id = station['station_id']
        status.is_returning = (station['is_returning'] == 1)
        status.is_renting = (station['is_renting'] == 1)
        status.is_installed = (station['is_installed'] == 1)
        status.docks_available = station['num_docks_available']
        status.bikes_available = station['num_bikes_available']
        db.session.add(status)


def update_system_station_info(system, existing_ids=None):
    if existing_ids is None:
        existing_ids = []
    if type(system) == str:
        system: BikeshareSystem = BikeshareSystem.query.get_or_404(system)
    station_feed = system.feed("station_information")["data"]["stations"]
    for station in station_feed:
        db_station = BikeshareStation()
        db_station.id = station['station_id']
        db_station.system_id = system.id
        db_station.name = station['name']
        db_station.address = station.get('address', station['name'])
        db_station.latitude = station['lat']
        db_station.longitude = station['lon']
        if BikeshareStation.query.get(db_station.id) is not None:
            continue
        db.session.add(db_station)
        log().debug("added station " + db_station.id)
        if db_station.id not in existing_ids:
            existing_ids.append(db_station.id)
    db.session.commit()
    return existing_ids


@bp.cli.command("get-status", help="Get station status for all known systems")
@with_appcontext
def cli_get_status():
    time = datetime.now().replace(second=0, microsecond=0)
    systems: List[BikeshareSystem] = BikeshareSystem.query.all()
    for system in systems:
        get_system_status(system, time)
    db.session.commit()


@bp.cli.command("purge-statuses", help="Remove ALL historical data for all known systems")
@with_appcontext
def cli_purge_statuses():
    BikeshareStationStatus.query.delete()
    db.session.commit()


@bp.get("/status")
def api_get_station_status():
    args = request.args
    systems = args.get("systems", type=split_by_comma)
    start_time = args.get("start", datetime.now() - timedelta(hours=6), type=dateutil.parser.isoparse)
    end_time = args.get("end", datetime.now(), type=dateutil.parser.isoparse)
    property_name = args.get("property", type=str)
    if systems is None:
        abort(400, "must pass systems to query")
    status_recs = BikeshareStationStatus.query \
        .join(BikeshareStationStatus.station) \
        .join(BikeshareStation.system) \
        .filter(BikeshareSystem.id.in_(systems)) \
        .filter(BikeshareStationStatus.time >= start_time) \
        .filter(BikeshareStationStatus.time <= end_time) \
        .all()
    statuses = {}
    for status in status_recs:
        key = status.time.strftime("%Y-%m-%dT%H:%M:%SZ")
        if key not in statuses:
            statuses[key] = {}
        statuses[key][status.station.name] = {
            'is_returning': status.is_returning,
            'is_renting': status.is_renting,
            'is_installed': status.is_installed,
            'docks_available': status.docks_available,
            'bikes_available': status.bikes_available
        }
    status_table = []
    for (time, status) in statuses.items():
        current_status = OrderedDict()
        current_status['time'] = time
        for (station_name, station_status) in status.items():
            if property_name is None:
                current_status[station_name] = station_status
            else:
                current_status[station_name] = station_status[property_name]
        status_table.append(current_status)
    return app.response_class(
        json.dumps(status_table, sort_keys=False),
        mimetype=app.config['JSONIFY_MIMETYPE'])


@bp.get("/summary")
def api_get_station_summary():
    args = request.args
    systems = args.get("systems", type=split_by_comma)
    if systems is None:
        abort(400, "must pass systems to query")
    last_ts = BikeshareStationStatus.most_recent_times_for(*systems)
    status_recs = BikeshareStationStatus.query \
        .join(BikeshareStationStatus.station) \
        .join(BikeshareStation.system)
    cond = db.or_(False)
    for (system, time) in last_ts.items():
        cond = db.or_(cond, db.and_(BikeshareSystem.id == system, BikeshareStationStatus.time == time))
    query_results = status_recs.filter(cond).all()
    aggregated_data = {
        'total_bikes_available': 0,
        'total_docks_available': 0,
        'total_installed': 0,
        'total_renting': 0,
        'total_returning': 0,
        'total_stations': len(query_results)
    }
    for station in query_results:
        station: BikeshareStationStatus
        aggregated_data['total_bikes_available'] += station.bikes_available
        aggregated_data['total_docks_available'] += station.docks_available
        if station.is_installed:
            aggregated_data['total_installed'] += 1
        if station.is_renting:
            aggregated_data['total_renting'] += 1
        if station.is_returning:
            aggregated_data['total_returning'] += 1
    aggregated_data['total_docks_active'] = aggregated_data['total_bikes_available'] + \
        aggregated_data['total_docks_available']
    return aggregated_data

