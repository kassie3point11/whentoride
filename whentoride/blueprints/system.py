import click
from flask import Blueprint, jsonify
from gbfs.services import SystemDiscoveryService

from ..db import db
from ..models.bikeshare_system import BikeshareSystem

ds = SystemDiscoveryService()
bp = Blueprint("system", __name__, url_prefix="/system")
bp.cli.help = "Add/remove GBFS systems and get data from known systems"


def import_system_by_id(system_id):
    basic_system_info = ds.get_system_by_id(system_id)
    remote_system = ds.instantiate_client(system_id)
    remote_sysinfo = remote_system.request_feed('system_information')['data']

    system = BikeshareSystem()
    system.id = system_id
    system.name = basic_system_info['Name']
    system.country_code = basic_system_info['Country Code']
    system.location = basic_system_info['Location']
    system.url = basic_system_info['URL']
    system.gbfs_url = basic_system_info['Auto-Discovery URL']
    system.phone = remote_sysinfo['phone_number']
    system.email = remote_sysinfo['email']
    system.timezone = remote_sysinfo['timezone']

    db.session.add(system)


@bp.cli.command("import", help="Import a GBFS system from the GBFS community feed list by system ID")
@click.argument("system_ids", nargs=-1)
def cli_import_system(system_ids):
    for system_id in system_ids:
        any_exist = BikeshareSystem.query.filter_by(id=system_id).count()
        if any_exist:
            print(f"system id {system_id} already exists in database, skipping")
            continue
        import_system_by_id(system_id)
    db.session.commit()


@bp.cli.command("remove", help="Remove a GBFS system by system ID")
@click.argument("system_ids", nargs=-1)
def cli_remove_system(system_ids):
    for system_id in system_ids:
        db.session.delete(BikeshareSystem.query.get(system_id))
    db.session.commit()


@bp.get("/")
def api_get_systems():
    return jsonify([sys.jsonable_dict for sys in BikeshareSystem.query.all()])
