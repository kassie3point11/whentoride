# this is it's own function since i use it for query arg parsing
def split_by_comma(str):
    if str is None or len(str) == 0:
        return None
    return str.split(',')
