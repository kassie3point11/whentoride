import logging

from flask import Flask
from .config import Config
from .db import db, migrate
from logging.config import dictConfig
from pathlib import Path
# disabling this here because we gotta have all the classes there for relationships
# to work, but we aren't using them all yet
# noinspection PyUnresolvedReferences
from .models import bikeshare_system, bikeshare_station, bikeshare_station_status


dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s][%(levelname)s] %(module)s: %(message)s',
    }},
    'root': {
        'level': 'INFO',
    }
})


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)

    # figure out default path for migrations from module path
    migrations_path = Path(__file__).parent / "migrations"
    migrate.init_app(app, db, migrations_path)
    from .blueprints import system, station, import_export
    app.register_blueprint(system.bp)
    app.register_blueprint(station.bp)
    app.register_blueprint(import_export.bp)

    return app


