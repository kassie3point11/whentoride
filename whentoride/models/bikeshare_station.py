from ..db import db


class BikeshareStation(db.Model):
    __tablename__ = "bikeshare_station"
    id = db.Column(db.String(256), primary_key=True, index=True)
    name = db.Column(db.String(256))
    address = db.Column(db.String(256))
    latitude = db.Column(db.DECIMAL)
    longitude = db.Column(db.DECIMAL)
    system_id = db.Column(db.String(256), db.ForeignKey("bikeshare_system.id"), index=True)

    system = db.relationship("BikeshareSystem", back_populates="stations")
    statuses = db.relationship("BikeshareStationStatus", back_populates="station")

    @property
    def jsonable_dict(self):
        self_dict = vars(self).copy()
        self_dict.pop("_sa_instance_state")
        self_dict['system'] = self.system.jsonable_dict
        return self_dict
