from ..db import db


class BikeshareStationStatus(db.Model):
    __tablename__ = "bikeshare_station_status"
    time = db.Column(db.TIMESTAMP, primary_key=True, index=True)
    station_id = db.Column(db.String(256), db.ForeignKey("bikeshare_station.id"), primary_key=True, index=True)
    is_returning = db.Column(db.Boolean)
    is_renting = db.Column(db.Boolean)
    is_installed = db.Column(db.Boolean)
    docks_available = db.Column(db.Integer)
    bikes_available = db.Column(db.Integer)

    station = db.relationship("BikeshareStation", back_populates="statuses")

    @property
    def jsonable_dict(self):
        self_dict = vars(self).copy()
        self_dict.pop("_sa_instance_state")
        self_dict['station'] = self.station.jsonable_dict
        return self_dict

    @staticmethod
    def most_recent_times_for(*args):
        systems = (*args,)
        query = db.text("""
            SELECT * FROM (
                SELECT
                    b.id as system_id,
                    bss.time as time,
                    row_number() over (PARTITION BY b.id ORDER BY bss.time desc) as row_num
                FROM bikeshare_station_status bss
                     LEFT JOIN bikeshare_station bs ON bs.id = bss.station_id
                     LEFT JOIN bikeshare_system b on b.id = bs.system_id
                WHERE b.id IN :systems
            ) x WHERE x.row_num <= 1;
            """)
        query = query \
            .bindparams(systems=systems) \
            .columns(system_id=db.String, time=db.TIMESTAMP, row_num=db.BIGINT)
        results = db.session.execute(query)
        result_dict = {}
        for row in results:
            result_dict.update({row[0]: row[1]})
        return result_dict
