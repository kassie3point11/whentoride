from ..db import db
from gbfs.client import GBFSClient


class BikeshareSystem(db.Model):
    __tablename__ = "bikeshare_system"
    id = db.Column(db.String(256), primary_key=True, index=True)
    name = db.Column(db.String(256))
    country_code = db.Column(db.String(2))
    location = db.Column(db.String(256))
    url = db.Column(db.String(256))
    gbfs_url = db.Column(db.String(256))
    phone = db.Column(db.String(16))
    email = db.Column(db.String(64))
    timezone = db.Column(db.String(64))

    stations = db.relationship("BikeshareStation", back_populates="system")

    @property
    def gbfs(self) -> GBFSClient:
        return GBFSClient(self.gbfs_url)

    def feed(self, name: str):
        return self.gbfs.request_feed(name)

    @property
    def jsonable_dict(self):
        self_dict = vars(self).copy()
        self_dict.pop('_sa_instance_state')
        return self_dict
