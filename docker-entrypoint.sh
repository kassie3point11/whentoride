#!/bin/sh

set -e

. /venv/bin/activate

while ! flask db upgrade
do
     echo "DB upgrade failed at $(date). Retrying in 30 seconds..."
     sleep 30
     echo "Retrying."
done
if [ "$1" = "run" ]; then
  exec gunicorn -b 0.0.0.0:80 "whentoride:create_app()"
fi
exec flask "$@"